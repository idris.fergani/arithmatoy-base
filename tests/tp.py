# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return "S" * n + "0"


def S(n: str) -> str:
    return "S" + n

def get_decimal_length(symbolic_number: str) -> int:
    length = 0
    for char in symbolic_number:
        if char == "S":
            length += 1
        else:
            break
        return length


def addition(a: str, b: str) -> str:
    if a == "0":
        result = b
    else:
        result = S(addition(a[1:], b))

    return result

def multiplication(a: str, b: str) -> str:
    if a == "0":
        result = "0"
    else:
        result = addition(b, multiplication(a[1:], b))

    return result


def facto_ite(n: int) -> int:
    return 1 if n == 0 else n * facto_ite(n - 1)

def facto_rec(n: int) -> int:
    return 1 if n == 0 else n * facto_rec(n - 1)

def fibo_rec(n: int) -> int:
    return n if n < 2 else fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    if n < 2:
        return n

    a, b = 0, 1

    for _ in range(n - 1):
        a, b = b, a + b

    return b


def golden_phi(n: int) -> float:
    if n == 0:
        raise ValueError("n doit etre superieur a 0")

    fib_values = [0, 1]

    for i in range(2, n + 1):
        fib_values.append(fib_values[i - 1] + fib_values[i - 2])

    return fib_values[-1] / fib_values[-2]

def sqrt5(n: int) -> int:
    pass


def pow(a: float, n: int) -> float:
    pass
