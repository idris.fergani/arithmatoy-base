#include <stddef.h>

#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char * get_all_digits() {
    return "0123456789abcdefghijklmnopqrstuvwxyz";
}
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char * number) {
    free(number);
}

// fonction qui permet d'ajout
char * arithmatoy_add(unsigned int base,
    const char * lhs,
    const char * rhs) {

    if (VERBOSE)
    {
        fprintf(stderr, "add: entering function\n");
    }

    // nettoyage des variables
    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);

    // calcul des longueurs
    size_t lhs_longueur = strlen(lhs);
    size_t rhs_longueur = strlen(rhs);

    // inversement des variables si la longueur de lhs est inférieure à la longueur de rhs
    if (lhs_longueur < rhs_longueur) {
        const char * tmp = lhs;
        const size_t tmp_longueur = lhs_longueur;
        lhs = rhs;
        rhs = tmp;
        lhs_longueur = rhs_longueur;
        rhs_longueur = tmp_longueur;
    }

    // allocation
    char * resultat = calloc(lhs_longueur + 1, sizeof(char));
    if (resultat == NULL) {
        debug_abort("add: calloc failed\n");
    }

    unsigned int lhs_nbr, rhs_nbr, somme;
    unsigned int carry = 0;
    int indice;
    for (size_t i = 0; i < lhs_longueur; ++i) {

        lhs_nbr = get_digit_value(lhs[lhs_longueur - i - 1]);
        indice = rhs_longueur - i - 1;
        if (i < rhs_longueur) {
            rhs_nbr = get_digit_value(rhs[indice]);
        } else {
            rhs_nbr = 0;
        }

        if (VERBOSE) {
            fprintf(stderr, "add: digit %c digit %c carry %u\n", to_digit(lhs_nbr), to_digit(rhs_nbr), carry);
        }

        // calcul de la nouvelle somme
        somme = lhs_nbr + rhs_nbr + carry;
        carry = somme / base;
        somme = somme % base;

        if (VERBOSE) {
            fprintf(stderr, "add: resultat: digit %c carry %u\n", to_digit(somme), carry);
        }

        resultat[lhs_longueur - i - 1] = to_digit(somme);
    }

    char * resultat_final;
    if (carry != 0) {

        if (VERBOSE) {
            fprintf(stderr, "add: final carry %u\n", carry);
        }

        resultat_final = calloc(lhs_longueur + 2, sizeof(char));
        resultat_final[0] = to_digit(carry);

        strcpy(resultat_final + 1, resultat);
        arithmatoy_free(resultat);

    } else {
        resultat_final = resultat;
    }

    return resultat_final;
}

char * arithmatoy_sub(unsigned int base,
    const char * lhs,
        const char * rhs) {

    if (VERBOSE) {
        fprintf(stderr, "sub: entering function\n");
    }

    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);
    const size_t lhs_longueur = strlen(lhs);
    const size_t rhs_longueur = strlen(rhs);

    // cas d'erreur
    if (lhs_longueur < rhs_longueur) {
        return NULL;
    }

    // allocation
    char * resultat = calloc(lhs_longueur + 1, sizeof(char));
    if (resultat == NULL) {
        debug_abort("sub: calloc failed\n");
    }

    unsigned int carry = 0;
    unsigned int lhs_nbr, rhs_nbr;
    int difference;
    int indice;
    for (size_t i = 0; i < lhs_longueur; ++i) {

        lhs_nbr = get_digit_value(lhs[lhs_longueur - i - 1]);
        indice = rhs_longueur - i - 1;
        if (i < rhs_longueur) {
            rhs_nbr = get_digit_value(rhs[indice]);
        } else {
            rhs_nbr = 0;
        }

        if (VERBOSE) {
            fprintf(stderr, "sub: digit %c digit %c carry %u\n", to_digit(lhs_nbr), to_digit(rhs_nbr), carry );
        }

        // calculs sur la differenceérence
        difference = lhs_nbr - rhs_nbr - carry;
        if (difference < 0) {
            carry = 1;
            difference += base;
        } else {
            carry = 0;
        }

        if (VERBOSE) {
            fprintf(stderr, "sub: resultat: digit %c carry %u\n", to_digit(difference), carry );
        }

        resultat[lhs_longueur - i - 1] = to_digit(difference);
    }

    if (carry != 0) {
        arithmatoy_free(resultat);
        return NULL;
    }

    char * resultat_old = resultat;
    resultat = (char * ) drop_leading_zeros(resultat);

    char * resultat_final = calloc(strlen(resultat) + 1, sizeof(char));

    strcpy(resultat_final, resultat);
    arithmatoy_free(resultat_old);

    return resultat_final;
}

char * arithmatoy_mul(unsigned int base,
    const char * lhs,
        const char * rhs) {

    if (VERBOSE) {
        fprintf(stderr, "mul: entering function\n");
    }

    lhs = drop_leading_zeros(lhs);
    rhs = drop_leading_zeros(rhs);
    size_t lhs_longueur = strlen(lhs);
    size_t rhs_longueur = strlen(rhs);

    if (lhs_longueur < rhs_longueur) {
        const char * tmp = lhs;
        const size_t tmp_longueur = lhs_longueur;
        lhs = rhs;
        rhs = tmp;
        lhs_longueur = rhs_longueur;
        rhs_longueur = tmp_longueur;
    }

    const size_t resultat_len = lhs_longueur + rhs_longueur;
    char * resultat = calloc(resultat_len + 1, sizeof(char));

    if (resultat == NULL) {
        debug_abort("mul: calloc failed\n");
    }

    char * partial_resultat = calloc(resultat_len + 1, sizeof(char));
    if (partial_resultat == NULL) {
        arithmatoy_free(resultat);
        debug_abort("mul: calloc failed\n");
    }

    // multiplication de chaque nombres
    resultat[0] = '0';
    unsigned int lhs_nbr, rhs_nbr, product, carry;

    for (size_t i = 0; i < rhs_longueur; ++i) {
        memset(partial_resultat, '0', resultat_len);

        carry = 0;
        rhs_nbr = get_digit_value(rhs[rhs_longueur - i - 1]);

        if (VERBOSE) {
            fprintf(stderr, "mul: digit %c number %s\n", to_digit(rhs_nbr), lhs);
        }

        for (size_t j = 0; j < lhs_longueur; ++j) {
            lhs_nbr = get_digit_value(lhs[lhs_longueur - j - 1]);

            if (VERBOSE) {
                fprintf(stderr, "mul: digit %c digit %c carry %u\n", to_digit(rhs_nbr), to_digit(lhs_nbr), carry );
            }

            product = lhs_nbr * rhs_nbr + carry;
            carry = product / base;
            product = product % base;

            if (VERBOSE) {
                fprintf(stderr, "mul: resultat: digit %c carry %u\n", to_digit(product), carry );
            }

            partial_resultat[resultat_len - j - i - 1] = to_digit(product);
        }

        if (carry != 0) {

            if (VERBOSE) {
                fprintf(stderr, "mul: final carry %c\n", to_digit(carry));
            }

            partial_resultat[resultat_len - lhs_longueur - i - 1] = to_digit(carry);
        }

        if (VERBOSE) {
            fprintf(stderr, "mul: add %s + %s\n", drop_leading_zeros(resultat), partial_resultat + resultat_len - lhs_longueur - i - (carry != 0) );
        }

        char * tmp = arithmatoy_add(base, resultat, partial_resultat);
        if (tmp == NULL) {
            debug_abort("mul: arithmatoy_add failed\n");
        }

        arithmatoy_free(resultat);
        resultat = tmp;

        if (VERBOSE) {
            fprintf(stderr, "mul: resultat: %s\n", resultat);
        }
    }

    arithmatoy_free(partial_resultat);

    return resultat;
}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
    // Convert a digit from get_all_digits() to its integer value
    if (digit >= '0' && digit <= '9') {
        return digit - '0';
    }
    if (digit >= 'a' && digit <= 'z') {
        return 10 + (digit - 'a');
    }
    return -1;
}

char to_digit(unsigned int value) {
    // Convert an integer value to a digit from get_all_digits()
    if (value >= ALL_DIGIT_COUNT) {
        debug_abort("Invalid value for to_digit()");
        return 0;
    }
    return get_all_digits()[value];
}

char * reverse(char * str) {
    // Reverse a string in place, return the pointer for convenience
    // Might be helpful if you fill your char* buffer from left to right
    const size_t length = strlen(str);
    const size_t bound = length / 2;
    for (size_t i = 0; i < bound; ++i) {
        char tmp = str[i];
        const size_t mirror = length - i - 1;
        str[i] = str[mirror];
        str[mirror] = tmp;
    }
    return str;
}

const char * drop_leading_zeros(const char * number) {
    // If the number has leading zeros, return a pointer past these zeros
    // Might be helpful to avoid computing a result with leading zeros
    if ( * number == '\0') {
        return number;
    }
    while ( * number == '0') {
        ++number;
    }
    if ( * number == '\0') {
        --number;
    }
    return number;
}

void debug_abort(const char * debug_msg) {
    // Print a message and exit
    fprintf(stderr, debug_msg);
    exit(EXIT_FAILURE);
}